<?php

/**
 * @file
 * Date Interval views filter handler.
 * This filter combines "from" and "to" field filters into a single filter
 */

/**
 * Filter to handle dates stored as a timestamp.
 */
class date_interval_handler_filter_date extends views_handler_filter {
  var $always_multiple = TRUE;

  function option_definition() {
    $options = parent::option_definition();

    $options['value'] = array(
      'contains' => array(
        'from' => array('default' => array('day' => 0, 'month' => 0)),
        'to' => array('default' => array('day' => 0, 'month' => 0)),
      ),
    );

    return $options;
  }

  /**
   * Provide a simple textfield for equality
   */
  function value_form(&$form, &$form_state) {
    $form['value'] = array(
      '#tree' => TRUE,
      '#prefix' => '<div class="container-inline">',
      '#suffix' => '</div>',
    );

    if (!empty($form_state['exposed'])) {
      $identifier = $this->options['expose']['identifier'];
    }

    foreach (array('from', 'to') as $type) {
      $form['value'][$type] = array(
        '#tree' => TRUE,
        '#prefix' => '<div class="form-item form-item-field-date-interval-' . $type . '">',
        '#suffix' => '</div>',
      );
      $form['value'][$type]['day'] = array(
        '#type' => 'select',
        '#title' => check_plain(t(drupal_ucfirst($type))),
        '#options' => array_merge(array(0 => t('- Any -')), drupal_map_assoc(range(1, 31))),
        '#default_value' => $this->value[$type]['day'],
      );
      $form['value'][$type]['month'] = array(
        '#type' => 'select',
        '#options' => array_merge(array(0 => t('- Any -')), drupal_map_assoc(range(1, 12))),
        '#default_value' => $this->value[$type]['month'],
      );
      if (!empty($form_state['exposed']) && !isset($form_state['input'][$identifier][$type])) {
        $form_state['input'][$identifier][$type] = $this->value[$type];
      }
    }
  }

  function admin_summary() {
    if (!empty($this->options['exposed'])) {
      return t('exposed');
    }

    $year = date('Y'); // current year

    // add from
    $from = implode('.', array($this->value['from']['day'], $this->value['from']['month'], $year));

    // add to
    if ($this->value['from']['month'] > $this->value['to']['month'] || // if 'from' month is bigger than 'to' month
      // if 'from' month and than 'to' month are equal, but 'from' day is bigger than or equal
      ($this->value['from']['month'] == $this->value['to']['month'] &&
        $this->value['from']['day'] >= $this->value['to']['day'])) {
      $year++; // then its next year
    }
    $to = implode('.', array($this->value['to']['day'], $this->value['to']['month'], $year));

    return t('@from - @to', array('@from' => $from, '@to' => $to));
  }

  /**
   * Do some minor translation of the exposed input
   */
  function accept_exposed_input($input) {
    if (empty($this->options['exposed'])) {
      return TRUE;
    }

    // rewrite the input value so that it's in the correct format so that
    // the parent gets the right data.
    if (!empty($this->options['expose']['identifier'])) {
      $value = &$input[$this->options['expose']['identifier']];
      if (!is_array($value)) {
        $value = array(
          'value' => $value,
        );
      }
    }

    if (empty($this->options['expose']['required']) && $value['from'] === '') {
      return FALSE;
    }

    return parent::accept_exposed_input($input);   ;
  }

  function query() {
    if ($this->value['from']['day'] || $this->value['from']['month'] ||
      $this->value['to']['day'] || $this->value['to']['month']) {

      $this->ensure_my_table(); // add LEFT JOIN

      $year = date('Y'); // current year
      $value_from = $this->value['from'];
      $value_to = $this->value['to'];

      // add from
      if ($value_from['day'] || $value_from['month']) { // if day or month is selected
        // prepare data
        $from_day = $value_from['day'] ? $value_from['day'] : 1;
        $from_month = $value_from['month'] ? $value_from['month'] : 1;
        $from_date = mktime(0, 0, 0, $from_month, $from_day, $year); // 'from' always uses current year

        // add query
        $this->query->add_where(
          $this->options['group'],
          "$this->table_alias.field_date_interval_from",
          $from_date,
          '>='
        );
      }

      // add to
      if ($value_to['day'] || $value_to['month']) {
        // prepare data
        $to_day = $value_to['day'] ? $value_to['day'] : 1;
        $to_month = $value_to['month'] ? $value_to['month'] : 1;
        if ($from_month > $to_month || // if 'from' month is bigger than 'to' month
          // if 'from' month and than 'to' month are equal, but 'from' day is bigger than or equal
          ($from_month == $to_month && $from_day >= $to_day)) {
          $year++; // then its next year
        }
        $to_date = mktime(0, 0, 0, $to_month, $to_day, $year); // 'from' always uses current year

        // add query
        $this->query->add_where(
          $this->options['group'],
          "$this->table_alias.field_date_interval_to",
          $to_date,
          '<='
        );
      }
    }
  }

}
